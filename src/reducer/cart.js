
const initialState = {
    cart: [],
    total: 0,
    openCart: false
}

const cartReducer = (state = initialState, action) => {
    switch(action.type) {
        case "SET_OPEN_CART" : {
            return {
                ...state,
                openCart: action.payload
            }
        }

        case "SET_CART" : {
            return {
                ...state,
                cart: action.payload
            }
        }

        case "ADD_TO_CART" : {
            let newList = [...state.cart];
            newList.push(action.payload);

            return {
                ...state,
                cart: newList
            }
        }

        case "REMOVE_FROM_CART": {
            let newList = state.cart.filter(element => element.id !== action.payload)

            return {
                ...state,
                cart: newList
            }
        }

        case "INCREASE_AMOUNT" : {
            const changeQuantity = element => {
                if (element.id === action.payload) {
                    element.quantity = element.quantity + 1
                }
                return element;
              };
        
            return {
                ...state,
                cart: state.cart.map(changeQuantity)
            }
        }   

        case "REDUCE_AMOUNT" : {
            const changeQuantity = element => {
                if (element.id === action.payload && element.quantity > 1) {
                        element.quantity = element.quantity - 1
                }
                return element;
              };
        
            return {
                ...state,
                cart: state.cart.map(changeQuantity)
            }
        }

        case "GET_TOTAL" : {
            return {
                ...state,
                total: action.payload
            }
        }
        default:
            return state;
    }
}

export default cartReducer;