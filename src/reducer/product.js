
const initialState = {
    productList : []
}

const productReducer = (state = initialState, action) => {
    switch(action.type){
        case "GET_ALL_PRODUCT" : {
            return {
                ...state,
                productList: action.payload
            }
        }

        default :
        return state
    }
}

export default productReducer