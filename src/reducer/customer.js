
const initialState = {
    customer: null,
    allCustomer: []
}

const customerReducer = (state = initialState, action) => {
    switch(action.type) {

        case "SET_CUSTOMER" : {
            return {
                ...state,
                customer: action.payload
            }
        }

        case "SET_ALL_ACCOUNT" : {
            return {
                ...state,
                allCustomer: action.payload
            }
        }

        default:
            return state;
    }
}

export default customerReducer;