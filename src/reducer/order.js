
const initialState = {
    allOrder: [],
    selectedOrder: {orderValue:0},
}

const orderReducer = (state = initialState, action) => {
    switch(action.type) {
        case "SET_ALL_ORDER": {
            
            return {
                ...state,
                allOrder: action.payload
            }
        }

        case "SET_SELECTED_ORDER": {
            return {
                ...state,
                selectedOrder: action.payload
            }
        }

        default:
            return state;
    }
}

export default orderReducer;