import {combineReducers} from "redux"
import cartReducer from "./cart"
import customerReducer from "./customer"
import orderReducer from "./order"
import productReducer from "./product"

const rootReducer = combineReducers({
    product: productReducer,
    customer: customerReducer,
    order: orderReducer,
    cart: cartReducer
})

export default rootReducer