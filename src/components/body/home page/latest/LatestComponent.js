import { Card, CardContent, CardMedia, Grid, Typography } from "@mui/material"
import { useNavigate } from "react-router-dom"

function LatestComponent({ productInPage }) {
    const navigation = useNavigate("")

    const onProductClick = (paramId) => {
        navigation(`/detail/${paramId}`)
    }

    return (
        <div>
            <h1 className="text-center" style={{ marginTop: "100px", marginBottom: "30px", fontWeight: "bolder" }}>LATEST PRODUCT</h1>
            <Grid container>
                {productInPage.map((element, index) => {
                    return <Grid item xs={12} lg={3} paddingLeft="30px" paddingBottom="20px" key={index}>
                    <Card onClick={() => onProductClick(element._id)}  sx={{ maxWidth: 230 }}>
                        <CardMedia component="img" height="250" image={element.imageUrl} />
                        <CardContent>
                            <Typography variant="caption" style={{ color: "grey", marginBottom: "10px" }}>{element.type} - {element.flavor} - {element.productOrigin} </Typography>
                            <Typography gutterBottom variant="h6" component="div">
                                {element.brand} - {element.name} {element.capacity}ML
                            </Typography>
                            <Grid container direction="row">
                                <Typography variant="body2" color="dark" paddingTop="7px" style={{ textDecoration: "line-through" }}>
                                    {element.buyPrice.toLocaleString()}
                                </Typography>
                                <Typography variant="h6" style={{ color: "red", marginLeft: "20px" }}>
                                    {element.promotionPrice.toLocaleString()}
                                </Typography>
                            </Grid>
                        </CardContent>
                    </Card>
                    </Grid>
                })}
            </Grid>
        </div>
    )
}

export default LatestComponent