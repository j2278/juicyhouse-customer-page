import { Container } from "reactstrap"
import CarouselComponent from "./carousel/CarouselComponent"
import LatestComponent from "./latest/LatestComponent"
import ViewAll from "./view all/ViewAllComponent"
import { useEffect } from "react"
import { useSelector } from "react-redux"

function getLastest(list) {
    let result = list.filter(element => element.lastest === true);
    return result
}

function HomePageComponent () {    
    const productList = useSelector(state => state.product.productList)

    const productInPage = getLastest(productList)

    useEffect(() => {
        document.title = `Shop24H | Home Page`;
    }, [])

    return (
        <div>
        <Container style={{marginTop:"10px"}} >
        <CarouselComponent></CarouselComponent>
        <LatestComponent productInPage={productInPage}></LatestComponent>
        <ViewAll></ViewAll>
        </Container>
        </div>
    )
}

export default HomePageComponent