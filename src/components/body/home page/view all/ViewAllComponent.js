import { Button } from "@mui/material"

function ViewAll () {

    return (
        <div className="text-center" style={{marginTop:"60px"}} >
            <Button variant="contained" style={{backgroundColor:"black"}} href="/products"><b>VIEW ALL</b></Button>
        </div>
    )
}

export default ViewAll