import Slider from "react-slick";
import "./Slider.css"
import SliderComponent1 from "./slider/SliderComponent1"
import SliderComponent2 from "./slider/SliderComponent2"
import SliderComponent3 from "./slider/SliderComponent3"


function CarouselComponent() {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    fade: true
  };

  return (
    <Slider {...settings}>
      <SliderComponent1></SliderComponent1>
      <SliderComponent2></SliderComponent2>
      <SliderComponent3></SliderComponent3>
    </Slider>
  )
}

export default CarouselComponent