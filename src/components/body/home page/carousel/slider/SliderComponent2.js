import Vape2 from "../../../../../assets/images/vape2.jpg"

const imageStyles = {
    width: "100%",
    height: "100%"
}

function SliderComponent2() {
    return (
        <div>
            <img src={Vape2} style={imageStyles}></img>
        </div>
    )
}

export default SliderComponent2