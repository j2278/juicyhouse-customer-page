import Vape3 from "../../../../../assets/images/vape3.jpg"

const imageStyles = {
    width: "100%",
    height: "100%"
}

function SliderComponent3() {
    return (
        <div>
            <img src={Vape3} style={imageStyles}></img>
        </div>
    )
}

export default SliderComponent3