import Vape1 from "../../../../../assets/images/vape1.jpg"
import {Button} from "@mui/material"
import {useNavigate} from "react-router-dom";

const titleStyles = {
    position: "absolute",
    top: "370px",
    left: "550px",
    color: "white"
}

const contentStyle = {
    position: "absolute",
    top: "420px",
    left: "550px",
    color: "white"
}

const btnStyles = {
    position: "absolute",
    top: "520px",
    left: "550px",
    color: "white",
    backgroundColor: "black"
}

const imageStyles = {
    width: "100%",
    height: "100%"
}

function SliderComponent1 () {
    const navigation = useNavigate();

    const onBtnClick = () => {
        navigation("/product")
    }

    return (
        <div style={{position:"relative"}}>
            <h3 style={titleStyles}>FLASH SALE</h3>
            <h5 style={contentStyle}>This month, Juicy House offers a discount <br/> on all products from 10-20% and free delivery <br/> within the city</h5>
            <Button onClick={onBtnClick} style={btnStyles} variant="contained">Shop now</Button>
            <img src={Vape1} style={imageStyles}></img>
        </div>
    )
}

export default SliderComponent1