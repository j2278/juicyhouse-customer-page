import { Card, Container, CardContent, CardMedia, Grid, Typography } from "@mui/material"
import SearchBreacrums from "./breadcrum/SearchBreadcrums"
import { useParams } from "react-router-dom"
import { useEffect, useState } from "react"
import axios from "axios"
import { useNavigate } from "react-router-dom"


function SearchResult() {
    const { keyword } = useParams()

    const [productInPage, setProductInPage] = useState([])

    const getAllProduct = async () => {
        const response = await axios(`http://localhost:8000/products`)
        const data = response.data;
        return data;
    }

    const navigation = useNavigate("")

    const onProductClick = (paramId) => {
        navigation(`/detail/${paramId}`)
    }


    useEffect(() => {
        console.log(keyword);
        getAllProduct()
            .then((data) => {
                setProductInPage(data.productList.filter(element => element.keyword.toLowerCase().includes(keyword)));
            })
            .catch((error) => {
                console.log(error.message)
            })
        window.scrollTo(0, 10)
    }, [keyword])

    return (
        <div style={{ paddingTop: "50px" }}>
            <SearchBreacrums></SearchBreacrums>
            <Container style={{ paddingTop: "50px" }}>
                {productInPage.length === 0 ?
                    <Typography className="text-center" variant="h5">No products found</Typography>
                    :
                    <Grid container style={{ paddingLeft: "50px" }} >
                        {productInPage.map((element, index) => {
                            return <Card onClick={() => onProductClick(element._id)} key={index} sx={{ maxWidth: 230 }} style={{ marginRight: "30px", marginBottom: "40px" }}>
                                <CardMedia component="img" height="250" image={element.imageUrl} />
                                <CardContent>
                                    <Typography variant="caption" style={{ color: "grey", marginBottom: "10px" }}>{element.type} - {element.flavor} - {element.productOrigin} </Typography>
                                    <Typography gutterBottom variant="h6" component="div">
                                        {element.brand} - {element.name} {element.capacity}ML
                                    </Typography>
                                    <Grid container direction="row">
                                        <Typography variant="body2" color="dark" paddingTop="7px" style={{ textDecoration: "line-through" }}>
                                            {element.buyPrice}
                                        </Typography>
                                        <Typography variant="h6" style={{ color: "red", marginLeft: "20px" }}>
                                            {element.promotionPrice}
                                        </Typography>
                                    </Grid>
                                </CardContent>
                            </Card>
                        })}
                    </Grid>}
            </Container>
        </div>
    )
}

export default SearchResult