import { Breadcrumbs, Grid, Link, Typography } from "@mui/material"

function SearchBreacrums() {
    return (
        <div>
            <Grid container>
                <Breadcrumbs separator=">>">
                    <Link underline="hover" color="inherit" href="/">
                        <b>Home</b>
                    </Link>
                    <Link underline="hover" color="inherit" href="/products">
                        <b>All Products</b>
                    </Link>
                    <Typography color="text.primary"><b>Search</b></Typography>
                </Breadcrumbs>
            </Grid>
        </div>
    )
}

export default SearchBreacrums