import { Breadcrumbs, Grid, Link, Typography } from "@mui/material"

function PaymentBreadcrums() {
    return (
        <div>
            <Grid container>
                <Breadcrumbs separator=">>">
                    <Link underline="hover" color="inherit" href="/">
                        <b>Home</b>
                    </Link>
                    <Typography color="text.primary"><b>Payment</b></Typography>
                </Breadcrumbs>
            </Grid>
        </div>
    )
}

export default PaymentBreadcrums