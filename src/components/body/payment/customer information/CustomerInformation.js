import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux"
import axios from "axios";
import { Button, Grid, Stack, TextField, Typography, Snackbar, Alert } from "@mui/material"

function CustomerInformation( {error, setError, errorDisplay, setErrorDisplay} ) {
    const customer = useSelector(state => state.customer.customer);
    const allCustomer = useSelector(state => state.customer.allCustomer);
    const dispatch = useDispatch()
    const [userInput, setUserInput] = useState({
        fullname: "",
        email: "",
        phone: "",
        address: "",
        city: "",
        country: ""
    })


    const [openAlert, setOpenAlert] = useState(false);

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenAlert(false);
    };

    const updateCustomer = async () => {
        const response = await axios(`http://localhost:8000/customers/${customer._id}`, {
            method: `PUT`,
            data: userInput,
            header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data;
    }

    const getAllCustomer = async () => {
        const response = await axios(`http://localhost:8000/customers`)
        const data = response.data;
        return data;
      }

    const getCustomer = async () => {
        const response = await axios(`http://localhost:8000/customers/${customer._id}`)
        const data = response.data;
        return data;
    }

    const onFullNameChange = (event) => {
        setUserInput({ ...userInput, fullname: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, fullname: "Full name is empty!" })
        } else {
            setError({ ...error, fullname: "none" })
        }
    }

    const onEmailChange = (event) => {
        setUserInput({ ...userInput, email: event.target.value })
        let emailListExist = allCustomer.filter(element => element.email === event.target.value && element._id !== customer._id)

        if (event.target.value === "") {
            setError({ ...error, email: "Email is empty!" })
        } else if (/\S+@\S+\.\S+/.test(event.target.value) === false) {
            setError({ ...error, email: "Email syntax is not valid!" })
        } else if ( emailListExist.length > 0 ){
            setError({ ...error, email: "This email is already exist!" })
        } else {
            setError({ ...error, email: "none" })
        }
    }

    const onPhoneChange = (event) => {
        setUserInput({ ...userInput, phone: event.target.value })
        let phonelListExist = allCustomer.filter(element => element.phone === event.target.value && element._id !== customer._id)

        if (event.target.value === "") {
            setError({ ...error, phone: "Phone number is empty!" })
        } else if (isNaN(event.target.value) || event.target.value.length !== 10) {
            setError({ ...error, phone: "Phone number must be a 10 digit number!" })
        } else if ( phonelListExist.length > 0 ){
            setError({ ...error, phone: "This phone number is already exist!" })
        } else {
            setError({ ...error, phone: "none" })
        }
    }

    const onAddressChange = (event) => {
        setUserInput({ ...userInput, address: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, address: "Address is empty!" })
        } else {
            setError({ ...error, address: "none" })
        }
    }

    const onCityChange = (event) => {
        setUserInput({ ...userInput, city: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, city: "City is empty!" })
        } else {
            setError({ ...error, city: "none" })
        }
    }

    const onCountryChange = (event) => {
        setUserInput({ ...userInput, country: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, country: "Country is empty!" })
        } else {
            setError({ ...error, country: "none" })
        }
    }

    const updateCustomerInfo = () => {
        setErrorDisplay("block");
        if (error.fullname === "none" && error.email === "none" && error.phone === "none" &&
            error.address === "none" && error.city === "none" && error.country === "none") {
            updateCustomer()
            .then((data) => {
                setOpenAlert(true);
                getCustomer()
                .then((data) => {
                    dispatch({ type: "SET_CUSTOMER", payload: data.customer })
                })
                getAllCustomer()
                .then((data) => {
                  dispatch({ type: "SET_ALL_ACCOUNT", payload: data.customerList });
                })
            })
            .catch((error) => {
                console.log(error.message)
            })
        }
    }

    useEffect(() => {
        if (customer) {
            let customerInfo = {
                fullname: customer.fullname,
                email: customer.email,
                phone: customer.phone,
                address: customer.address,
                city: customer.city,
                country: customer.country
            }
            setUserInput(customerInfo);

            let errorFree = {
                fullname: "none",
                email:  "none",
                phone: "Phone number is empty!",
                address: "Address is empty!",
                city: "City is empty!",
                country: "Country is empty!"
            }
            if(customerInfo.phone !== ""){
                errorFree.phone = "none"
            }
            if(customerInfo.address !== ""){
                errorFree.address = "none"
            }
            if(customerInfo.city !== ""){
                errorFree.city = "none"
            }
            if(customerInfo.country !== ""){
                errorFree.country = "none"
            }
            setError(errorFree)
        } 
    }, [customer])

    return (
        <div>
            <Typography variant="h6" fontWeight="bolder">Customer Information</Typography>
            <Stack spacing={2} style={{ paddingRight: "30px", marginTop: "20px" }}>
                <TextField onChange={onFullNameChange} fullWidth size="small" variant="outlined" label="Full name" value={userInput.fullname}></TextField>
                {error.fullname === "none" ? null : <Typography style={{ color: "red", display: errorDisplay, marginTop: 5 }} variant="caption">{error.fullname}</Typography>}

                <Grid container>
                    <Grid item xs={8}>
                        <TextField onChange={onEmailChange} style={{ paddingRight: "10px" }} fullWidth size="small" variant="outlined" label="Email" value={userInput.email}></TextField>
                        {error.email === "none" ? null : <Typography style={{ color: "red", display: errorDisplay, marginTop: 5 }} variant="caption">{error.email}</Typography>}
                    </Grid>
                    <Grid item xs={4}>
                        <TextField onChange={onPhoneChange} fullWidth size="small" variant="outlined" label="Phone number" value={userInput.phone}></TextField>
                        {error.phone === "none" ? null : <Typography style={{ color: "red", display: errorDisplay, marginTop: 5 }} variant="caption">{error.phone}</Typography>}
                    </Grid>
                </Grid>

                <TextField onChange={onAddressChange} fullWidth size="small" variant="outlined" label="Address" value={userInput.address}></TextField>
                {error.address === "none" ? null : <Typography style={{ color: "red", display: errorDisplay, marginTop: 5 }} variant="caption">{error.address}</Typography>}

                <Grid container>
                    <Grid item xs={6}>
                        <TextField onChange={onCityChange} style={{ paddingRight: "10px" }} fullWidth size="small" variant="outlined" label="City" value={userInput.city}></TextField>
                        {error.city === "none" ? null : <Typography style={{ color: "red", display: errorDisplay, marginTop: 5 }} variant="caption">{error.city}</Typography>}
                    </Grid>
                    <Grid item xs={6}>
                        <TextField onChange={onCountryChange} fullWidth size="small" variant="outlined" label="Country" value={userInput.country}></TextField>
                        {error.country === "none" ? null : <Typography style={{ color: "red", display: errorDisplay, marginTop: 5 }} variant="caption">{error.country}</Typography>}
                    </Grid>
                </Grid>
                    <Button variant="contained" style={{ fontWeight: "bold" }} onClick={updateCustomerInfo}>Update customer information</Button>
            </Stack>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                   Update successful
                </Alert>
            </Snackbar>
        </div>
    )
}

export default CustomerInformation