import { Card, CardMedia, Grid, Typography, Stack, TextField, Divider, Button, Modal, Box } from "@mui/material"
import axios from "axios";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux"
import { useNavigate } from "react-router-dom";


const circleStyle = {
    width: "35px",
    height: "35px",
    borderRadius: "50%",
    backgroundColor: "#2ECC71",
    color: "white",
    position: "relative",
    top: "-115px",
    left: "122px",
    fontWeight: "bolder",
    fontSize: "large",
    paddingTop: "3px"
}

const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 500,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function OrderInformation({ error, setErrorDisplay }) {
    const dispatch = useDispatch();
    const navigation = useNavigate();
    const cart = useSelector(state => state.cart.cart);
    const total = useSelector(state => state.cart.total);
    const customer = useSelector(state => state.customer.customer);
    const [note, setNote] = useState("");
    const [openModal, setOpenModal] = useState(false);
    const [newCreatedOrder, setNewCreatedOrder] = useState()

    const handleCloseModal = () => setOpenModal(false);


    const onNoteChange = (event) => {
        setNote(event.target.value)
    }

    const createOrder = async () => {
        let body = {
            orderValue: total,
            note: note
        }

        const response = await axios(`http://localhost:8000/customers/${customer._id}/orders`, {
            method: `POST`,
            data: body,
            header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data
    }

    const createOrderDetail = async (paramCart, paramOrder) => {
        let body = {
            quantity: paramCart.quantity,
            priceEach: paramCart.product.promotionPrice
        }

        const response = await axios(`http://localhost:8000/orders/${paramOrder._id}/orderdetails/${paramCart.product._id}`, {
            method: `POST`,
            data: body,
            header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data
    }

    const getAllCustomerOrders = async (paramCustomer) => {
        const response = await axios(`http://localhost:8000/customers/${paramCustomer._id}/orders`)
        const data = response.data;
        return data;
    }

    const onCreateOrderClick = () => {
        setErrorDisplay("block");
        if (error.fullname === "none" && error.email === "none" && error.phone === "none" &&
            error.address === "none" && error.city === "none" && error.country === "none") {
            createOrder()
                .then((data) => {
                    setOpenModal(true);
                    dispatch({ type: "SET_CART", payload: [] });
                    setNewCreatedOrder(data.newOrder);
                    for (let bI = 0; bI < cart.length; bI++) {
                        createOrderDetail(cart[bI], data.newOrder)
                            .then((data) => {
                                console.log(data.newOrderDetail)
                            })
                            .catch((error) => {
                                console.log(error.message)
                            })
                    }
                    getAllCustomerOrders(customer)
                        .then((data) => {
                            dispatch({ type: "SET_ALL_ORDER", payload: data.customerOrders })
                        })
                })
                .catch((error) => {
                    console.log(error.message)
                })
        } else {
            window.scrollTo(0,10)
        }
    }

    const onViewOrderClick = () => {
        handleCloseModal();
        navigation("/order")
        dispatch({ type: "SET_SELECTED_ORDER", payload: newCreatedOrder })
    }

    const onHomePageClick = () => {
        handleCloseModal();
        navigation("/")
    }

    return (
        <div style={{ paddingLeft: "30px", borderLeft: "2px solid #839192" }} >
            <Typography variant="h6" fontWeight="bolder">Order Information</Typography>
            <Stack divider={<Divider orientation="vertical" flexItem />}>
                <Stack spacing={2} style={{ marginTop: "20px" }}>
                    {cart.map((element, index) => {
                        return <Grid container key={index} >
                            <Grid item xs={3}>
                                <Card> <CardMedia component="img" height="100" src={element.product.imageUrl}></CardMedia> </Card>
                                <div style={circleStyle} className="text-center">{element.quantity}</div>
                            </Grid>
                            <Grid item xs={6} marginLeft="60px">
                                <Stack>
                                    <Typography fontWeight="bold" variant="subtitle1">{element.product.brand} {element.product.name} {element.product.capacity}ML</Typography>
                                    <Typography fontWeight="bold"> {(element.product.promotionPrice * element.quantity).toLocaleString()} VND</Typography>
                                </Stack>
                            </Grid>
                        </Grid>
                    })}
                </Stack>
                <Grid container className="my-3">
                    <Grid item xs={12} marginBottom="10px">
                        <TextField value={note} onChange={onNoteChange} fullWidth variant="outlined" label="Note" size="small"></TextField>
                    </Grid>
                    <Grid item xs={10}>
                        <TextField fullWidth variant="outlined" label="Voucher" size="small"></TextField>
                    </Grid>
                    <Grid item xs={1} style={{ marginLeft: "10px" }}>
                        <Button style={{ backgroundColor: "black", fontWeight: "bold" }} variant="contained">Apply</Button>
                    </Grid>
                </Grid>
                <Stack className="my-2" >
                    <Grid container>
                        <Grid item xs={6}>
                            <Typography fontWeight="bolder">Order Price:</Typography>
                        </Grid>
                        <Grid item xs={6} style={{ textAlign: "right" }}>
                            <Typography fontWeight="bolder"> {total.toLocaleString()} </Typography>
                        </Grid>
                    </Grid>
                    <Grid container marginTop="5px">
                        <Grid item xs={6}>
                            <Typography fontWeight="bolder">Shipping Price:</Typography>
                        </Grid>
                        <Grid item xs={6} style={{ textAlign: "right" }}>
                            <Typography fontWeight="bolder"> 0 </Typography>
                        </Grid>
                    </Grid>
                </Stack>
                <Grid container className="my-2">
                    <Grid item xs={6}>
                        <Typography fontWeight="bolder">Total:</Typography>
                    </Grid>
                    <Grid item xs={6} style={{ textAlign: "right" }}>
                        <Typography fontWeight="bolder"> {total.toLocaleString()} </Typography>
                    </Grid>
                    <Grid item xs={12} style={{ textAlign: "right", marginTop: "10px" }}>
                        <Button onClick={onCreateOrderClick} color="success" style={{ fontWeight: "bold" }} variant="contained">Create order</Button>
                    </Grid>
                </Grid>
            </Stack>
            <Modal open={openModal}>
                <Box sx={modalStyle}>
                    <Typography id="modal-modal-title" variant="h6" component="h2" className="text-center" marginBottom="20px">
                        Your order has been successfully created
                    </Typography>
                    <Button onClick={onViewOrderClick} style={{ width: "140px" }} color="success" variant="contained" className="mx-5">View order</Button>
                    <Button onClick={onHomePageClick} variant="contained" style={{ width: "140px" }}>Home page</Button>
                </Box>
            </Modal>
        </div>
    )
}

export default OrderInformation