import { Grid } from "@mui/material"
import PaymentBreadcrums from "./breadcrums/PaymentBreadcrum"
import CustomerInformation from "./customer information/CustomerInformation";
import OrderInformation from "./order information/OrderInformation";
import { useState, useEffect } from "react"

function PaymentPage() {
    const [errorDisplay, setErrorDisplay] = useState("none")

    const [error, setError] = useState({
        fullname: "Full name is empty!",
        email:  "Email is empty!",
        phone: "Phone number is empty!",
        address: "Address is empty!",
        city: "City is empty!",
        country: "Country is empty!"
    })

    useEffect(() => {
        window.scrollTo(0,5)
    },[])
    return (
        <div style={{ marginTop: "20px" }}>
            <PaymentBreadcrums></PaymentBreadcrums>
            <Grid container marginTop="30px" >
                <Grid item xs={12} lg={6} marginBottom="30px">
                    <CustomerInformation error={error} setError={setError} errorDisplay={errorDisplay} setErrorDisplay={setErrorDisplay}></CustomerInformation>
                </Grid>
                <Grid item xs={12} lg={6}>
                    <OrderInformation error={error} setErrorDisplay={setErrorDisplay}></OrderInformation>
                </Grid>
            </Grid>
        </div>
    )
}

export default PaymentPage