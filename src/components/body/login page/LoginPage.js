import { Stack, TextField, Typography, Snackbar, Alert } from "@mui/material";
import GoogleIcon from '@mui/icons-material/Google';
import { auth, googleProvider } from "../../../firebase"
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import axios from "axios"
import { useState, useEffect } from "react"

const buttonGoogle = {
    backgroundColor: "#EE4000",
    padding: "10px",
    color: "white",
    borderRadius: "30px",
    fontSize: "large",
    fontWeight: "bold"
}

const buttonLogin = {
    backgroundColor: "#008B45",
    padding: "10px",
    color: "white",
    borderRadius: "30px",
    fontSize: "large",
    fontWeight: "bold",
}

const loginForm = {
    width: "300px",
    margin: "50px auto",
    padding: "0",
    marginTop:"120px"
}

function checkCustomer(customerList, userEmail) {
    let result = null;
    let bI = 0;
    let isFound = false;
    while (!isFound && bI < customerList.length) {
        if (userEmail === customerList[bI].email) {
            result = customerList[bI];
            isFound = true;
        } else {
            bI++
        }
    }
    return result
}

function LoginPage() {
    const dispatch = useDispatch();
    const navigation = useNavigate();
    const allCustomer = useSelector(state => state.customer.allCustomer);
    const [account, setAccount] = useState("")
    const [openAlert, setOpenAlert] = useState(false);

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenAlert(false);
    };


    const onAccountChange = (event) => {
        setAccount(event.target.value)
    }

    const onLoginClick = () => {
        let check = allCustomer.filter(element => element.email === account)
        if (check.length > 0) {
            navigation("/")
            dispatch({ type: "SET_CUSTOMER", payload: check[0] });
            dispatch({ type: "SET_CART", payload: check[0].cartList });
            getAllCustomerOrders(check[0])
                .then((data) => {
                    dispatch({ type: "SET_ALL_ORDER", payload: data.customerOrders })
                })
        } else {
            setOpenAlert(true)
        }
    }

    const getAllCustomer = async () => {
        const response = await axios(`http://localhost:8000/customers`)
        const data = response.data;
        return data;
    }

    const createCustomer = async (paramBody) => {
        const response = await axios(`http://localhost:8000/customers`, {
            method: `POST`,
            data: paramBody,
            header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data;
    }

    const getAllCustomerOrders = async (paramCustomer) => {
        const response = await axios(`http://localhost:8000/customers/${paramCustomer._id}/orders`)
        const data = response.data;
        return data;
    }

    const googleLogin = () => {
        auth.signInWithPopup(googleProvider)
            .then((result) => {
                navigation("/")
                let customerAccount = checkCustomer(allCustomer, result.user.email);
                if (customerAccount) {
                    dispatch({ type: "SET_CUSTOMER", payload: customerAccount });
                    dispatch({ type: "SET_CART", payload: customerAccount.cartList });
                    getAllCustomerOrders(customerAccount)
                        .then((data) => {
                            dispatch({ type: "SET_ALL_ORDER", payload: data.customerOrders })
                        })
                } else {
                        let body = {
                            fullname: result.user.displayName,
                            email: result.user.email,
                            avatar: result.user.photoURL
                        }
                        createCustomer(body)
                        .then((data) => {
                            dispatch({ type: "SET_CUSTOMER", payload: data.newCustomer });
                            //Gọi mới all customer để validate khi sign up
                            getAllCustomer()
                                .then((data) => {
                                    dispatch({ type: "SET_ALL_ACCOUNT", payload: data.customerList });
                                })
                        })
                    }
            })
    }

    const signUpClick = () => {
        navigation("/signup")
    }


    return (
        <div style={loginForm}>
            <Stack spacing={2} justifyContent="center" className="text-center">
                <Typography type="button" style={buttonGoogle} onClick={googleLogin}>
                    <GoogleIcon style={{ marginBottom: "3px", marginRight: "4px" }} /> Log in with Google
                </Typography>
                <Typography fontWeight="bold">OR</Typography>
                <TextField onChange={onAccountChange} size="small" fullWidth label="Username"></TextField>
                <TextField size="small" fullWidth label="Password"></TextField>
                <Typography onClick={onLoginClick} type="button" style={buttonLogin}>Log In</Typography>
                <Typography>Don't have any account? <span type="button" onClick={signUpClick} style={{ color: "blue" }}>Sign up here</span></Typography>
            </Stack>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                    User is not exist
                </Alert>
            </Snackbar>
        </div>
    )
}

export default LoginPage;