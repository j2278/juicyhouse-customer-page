import { Container, Grid, Card, CardMedia, Stack, Typography, Button, Snackbar, Alert, Modal, Box } from "@mui/material"
import { useParams, useNavigate } from "react-router-dom"
import { useEffect, useState } from "react"
import DetailBreadcrum from "./breadcrum/DetailBreadcrum";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import RelativeProducts from "./relative products/RelativeProducts";
import axios from "axios"
import { useSelector, useDispatch } from "react-redux";

const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function getId (cartList) {
    let id = 0;
    if(cartList.length === 0){
        id = 1;
    } else {
        id = cartList[cartList.length - 1].id + 1
    }

    return id;
}

function ProductDetail() {
    const productList = useSelector(state => state.product.productList);
    const cart = useSelector(state => state.cart.cart);
    const customer = useSelector(state => state.customer.customer);

    const { id } = useParams()
    const dispatch = useDispatch();

    const [productDetail, setProductDetail] = useState({promotionPrice:0})
    const [relativeProducts, setRelativeProduct] = useState([])

    const [amount, setAmount] = useState(1)

    const [openModal, setOpenModal] = useState(false);
    const handleCloseModal = () => setOpenModal(false);
    const navigation = useNavigate()

    const onLogInClick = () => {
        navigation("/login");
        handleClose()
    }

    const onSignUpClick = () => {
        navigation("/signup");
        handleClose()
    }

    const onRemoveClick = () => {
        if (amount === 1) {
            setOpen(true);
        } else {
            setAmount(amount - 1)
        }
    }
    
    const [open, setOpen] = useState(false);

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const getProduct = async () => {
        const response = await axios(`http://localhost:8000/products/${id}`)
        const data = response.data;
        return data;
    }



    const addToCartClick = () => {
        if(customer){
            dispatch({ type: "SET_OPEN_CART", payload: true });

            const productSelected = {
                id: getId(cart),
                product: productDetail,
                quantity: amount
            }
    
            dispatch({type:"ADD_TO_CART", payload: productSelected})
        } else {
            setOpenModal(true)
        }
    }

    useEffect(() => {
        window.scrollTo(0, 20);
        document.title = `Shop24H | Details`;
    }, [id])

    useEffect(() => {
        getProduct()
            .then((data) => {
                setProductDetail(data.product);
                setRelativeProduct(productList.filter(element => (element.type === productDetail.type || element.flavor === productDetail.flavor)
                && element._id !== id ).slice(0,10))
            })
    }, [productDetail]);



    return (
        <Container style={{ marginTop: "100px" }}>
            <DetailBreadcrum productDetail={productDetail}></DetailBreadcrum>
            <Grid container style={{ marginTop: "30px" }}>
                <Grid item xs={4}>
                    <Card>
                        <CardMedia component="img" height="450" image={productDetail.imageUrl} />
                    </Card>
                </Grid>
                <Grid item xs={8} style={{ paddingLeft: "30px", paddingTop: "20px" }}>
                    <Stack spacing={2}>
                        <Typography variant="h4" fontWeight="700">{productDetail.brand} {productDetail.name} {productDetail.capacity}ML </Typography>
                        <Typography><span style={{ color: "gray" }}>Brand:</span> {productDetail.brand}</Typography>
                        <Typography><span style={{ color: "gray" }}>Type:</span> {productDetail.type}</Typography>
                        <Typography><span style={{ color: "gray" }}>Flavor:</span> {productDetail.flavor}</Typography>
                        <Typography><span style={{ color: "gray" }}>Origin:</span> {productDetail.productOrigin}</Typography>
                        <Typography>{productDetail.description}</Typography>
                        <Typography variant="h6" fontWeight="bold" style={{ color: "#DD0000" }}>{productDetail.promotionPrice.toLocaleString()} VND</Typography>
                        <Typography>
                            <RemoveCircleIcon onClick={onRemoveClick} style={{ marginRight: "20px" }} />
                            <span style={{ fontSize: "20px" }}>{amount}</span>
                            <AddCircleIcon onClick={() => setAmount(amount + 1)} style={{ marginLeft: "20px" }} />
                        </Typography>
                        <Button onClick={addToCartClick} variant="contained" style={{ width: "200px", backgroundColor: "black" }}>Add to cart</Button>
                    </Stack>
                </Grid>
            </Grid>
            <Stack spacing={2} className="text-center" style={{ marginTop: "50px", textAlign:"center" }} >
                <Typography variant="h4" fontWeight="bold" >Description</Typography>
                <Typography>{productDetail.description}</Typography>
                <img src={productDetail.imageUrl} style={{ width: "500px", height: "500px", margin: "0 auto" }}></img>
            </Stack>
            <Stack spacing={2} className="text-center" style={{ marginTop: "50px" }}>
                <Typography variant="h4" fontWeight="bold">Related Products</Typography>
                <RelativeProducts relativeProducts={relativeProducts}></RelativeProducts>
            </Stack>
            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="warning" sx={{ width: '100%' }}>
                    The number of product cannot be smaller than 1
                </Alert>
            </Snackbar>
            <Modal open={openModal} onClose={handleCloseModal}>
                    <Box sx={modalStyle}>
                        <Typography id="modal-modal-title" variant="h6" component="h2" className="text-center" marginBottom="20px">
                            You have to sign in to create and pay for the order! <br /> Please click "LOG IN" to log in or <br /> "SIGN UP" to create new account
                        </Typography>
                        <Button onClick={onLogInClick} style={{ width: "120px" }} color="success" variant="contained" className="mx-4">LOG IN</Button>
                        <Button onClick={onSignUpClick} variant="contained" style={{ width: "120px" }}>SIGN UP</Button>
                    </Box>
                </Modal>
        </Container>
    )
}

export default ProductDetail