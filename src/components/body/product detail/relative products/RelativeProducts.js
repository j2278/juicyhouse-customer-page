import { Card, CardContent, CardMedia, Grid, Typography, Container } from "@mui/material"
import { useNavigate } from "react-router-dom"
import Slider from "react-slick";

function RelativeProducts({ relativeProducts }) {
    const navigation = useNavigate()

    const relativeProductClick = (paramId) => {
        navigation(`/detail/${paramId}`)
    }

    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        centerMode: true
    };

    return (
        <Grid container>
            <Container>
                <Slider {...settings}>
                    {relativeProducts.map((element, index) => {
                        return <Card key={index} onClick={() => relativeProductClick(element._id)} className="mx-2">
                            <CardMedia component="img" height="230" image={element.imageUrl} />
                            <CardContent>
                                <Typography variant="h6">{element.brand}</Typography>
                                <Typography variant="h6">{element.name} {element.capacity}ML</Typography>
                                <Typography style={{ marginTop: "10px" }}>
                                    <span style={{ textDecoration: "line-through" }}>{element.buyPrice.toLocaleString()}</span>
                                    <span style={{ color: "red", fontSize: "large", marginLeft: "20px" }}>{element.promotionPrice.toLocaleString()}</span>
                                </Typography>
                            </CardContent>
                        </Card>
                    })}

                </Slider>

            </Container>
        </Grid>
    )
}

export default RelativeProducts