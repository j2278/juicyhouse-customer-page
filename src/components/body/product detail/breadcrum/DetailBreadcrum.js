import { Breadcrumbs, Grid, Link, Typography } from "@mui/material"

function DetailBreadcrum( {productDetail} ) {
    return (
        <div>
            <Grid container>
                <Breadcrumbs separator=">>">
                    <Link underline="hover" color="inherit" href="/">
                        <b>Home</b>
                    </Link>
                    <Link underline="hover" color="inherit" href="/products">
                        <b>All products</b>
                    </Link>
                    <Typography color="text.primary"><b> {productDetail.brand} {productDetail.name} {productDetail.capacity}ML </b></Typography>
                </Breadcrumbs>
            </Grid>
        </div>
    )
}

export default DetailBreadcrum