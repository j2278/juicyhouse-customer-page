import { Button, Table, TableHead, TableBody, TableRow, TableCell } from "@mui/material"
import { useDispatch, useSelector } from "react-redux"

function OrderTable({ renderOrderStatus, orderInPage }) {
    const dispatch = useDispatch();


    const onViewClick = (element) => {
        dispatch({ type: "SET_SELECTED_ORDER", payload: element })
    }

    return (
        <div>
                <Table >
                    <TableHead >
                        <TableRow>
                            <TableCell><b>No</b></TableCell>
                            <TableCell><b>Date created</b></TableCell>
                            <TableCell><b>Order value</b></TableCell>
                            <TableCell><b>Status</b></TableCell>
                            <TableCell><b>Delivery date</b></TableCell>
                            <TableCell><b>View order</b></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {orderInPage.map((element, index) => {
                            return <TableRow key={index}>
                                <TableCell>{index + 1}</TableCell>
                                <TableCell>{element.timeCreated}</TableCell>
                                <TableCell>{element.orderValue.toLocaleString()}</TableCell>
                                <TableCell>{renderOrderStatus(element.orderStatus)}</TableCell>
                                <TableCell>{element.orderStatus === 0 ? "Not yet delivered" : element.shippedDate}</TableCell>
                                <TableCell>
                                    <Button onClick={() => onViewClick(element)} style={{ backgroundColor: "black", fontWeight: "bold" }} variant="contained">View</Button>
                                </TableCell>
                            </TableRow>
                        })}
                    </TableBody>
                </Table>

        </div>
    )
}

export default OrderTable