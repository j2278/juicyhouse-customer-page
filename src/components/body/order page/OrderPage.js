import { Container, Grid, ToggleButton, ToggleButtonGroup } from "@mui/material"
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import DirectionsCarIcon from '@mui/icons-material/DirectionsCar';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import OrderTable from "./order table/OrderTable";
import OrderDetail from "./order detail/OrderDetail";

const btnStyle = {
    width: "350px",
    color: "black",
    fontWeight: "bold",
}


function OrderPage() {
    const dispatch = useDispatch()
    const [alignment, setAlignment] = useState("all");
    const selectedOrder = useSelector(state => state.order.selectedOrder);
    const allOrder = useSelector(state => state.order.allOrder);
    const [orderInPage, setOrderInPage] = useState([])

    const renderOrderStatus = (status) => {
        if (status === 0) {
            return status = <span style={{ color: "blue" }}> <DirectionsCarIcon style={{ paddingBottom: "3px" }} /> Delivering</span>
        }
        if (status === 1) {
            return status = <span style={{ color: "green" }}> <CheckCircleOutlineIcon style={{ paddingBottom: "3px" }} />Delivered</span>
        }
        return status;
    }

    const handleAlignment = (event, newAligment) => {
        setAlignment(newAligment)
    }

    const onDeliveringClick = () => {
        let result = allOrder.filter(element => element.orderStatus === 0)
        setOrderInPage(result)
    }

    const onAllClick = () => {
        setOrderInPage(allOrder) 
    }

    const onDeliveredClick = () => {
        let result = allOrder.filter(element => element.orderStatus === 1)
        setOrderInPage(result) 
    }

    useEffect(() => {
        setOrderInPage(allOrder)
    },[])

    return (
        <div>
            <Container style={{ marginTop: "50px" }}>
                <Grid container style={{ marginLeft: "20px", marginBottom: "30px" }}>
                    <ToggleButtonGroup color='info' value={alignment} exclusive onChange={handleAlignment}>
                        <ToggleButton style={btnStyle} onClick={onAllClick} value="all"> All order </ToggleButton>
                        <ToggleButton style={btnStyle} onClick={onDeliveringClick} value="delivering"> Delivering </ToggleButton>
                        <ToggleButton style={btnStyle} onClick={onDeliveredClick} value="delivered"> Delivered </ToggleButton>
                    </ToggleButtonGroup>
                </Grid>
                <OrderTable orderInPage={orderInPage} renderOrderStatus={renderOrderStatus}></OrderTable>
            </Container>
            {selectedOrder ? <OrderDetail renderOrderStatus={renderOrderStatus}></OrderDetail> : null }
        </div>
    )
}

export default OrderPage