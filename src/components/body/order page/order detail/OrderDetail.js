import { Drawer, Typography, List, Grid, Stack } from "@mui/material"
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux"
import axios from "axios"
import CloseIcon from '@mui/icons-material/Close';


const listStyle = {
    width: "400px",
    textAlign: "center",
    paddingRight: "20px",
    marginTop:"20px"

}

function OrderDetail({ renderOrderStatus }) {
    const dispatch = useDispatch();

    const selectedOrder = useSelector(state => state.order.selectedOrder);
    const productList = useSelector(state => state.product.productList);

    const [orderDetail, setOrderDetail] = useState([])
    const [openView, setOpenView] = useState(false);


    const getAllOrderDetailOfOrder = async () => {
        const response = await axios(`http://localhost:8000/orders/${selectedOrder._id}/orderdetails`)
        const data = response.data;
        return data;
    }

    const getOrderProduct = (element) => {
        let orderDetailProduct = null;
        let isFound = false;
        let bI = 0;
        while (!isFound && bI < productList.length) {
            if (element.product == productList[bI]._id) {
                orderDetailProduct = productList[bI];
                isFound = true;
            } else {
                bI++;
            }
        }

        return orderDetailProduct;
    }

    const handleClose = () => {
        dispatch({ type: "SET_SELECTED_ORDER", payload: null })
        setOpenView(false)
    }

    useEffect(() => {
        if (selectedOrder) {
            getAllOrderDetailOfOrder()
                .then((data) => {
                    setOrderDetail(data.list);
                    setOpenView(true);
                })
        }
    }, [selectedOrder])

    return (
        <div>
            <Drawer open={openView} anchor="left" onClose={handleClose}>
                <Grid container>
                <Grid item xs={12} sx={{textAlign:"left"}}><CloseIcon  onClick={handleClose}/></Grid>
                </Grid>

                <Typography fontWeight="bold" className="text-center border-bottom" variant="h4">Order Details</Typography>
                <List sx={listStyle}>
                    <Stack spacing={1} style={{textAlign:"center"}}>
                        <Grid container>
                            <Grid item xs={12}>
                                <Grid container>
                                    <Grid item xs={6}>
                                        <Typography>Created date:</Typography>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography fontWeight="bold">{selectedOrder.timeCreated}</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                            </Grid>
                            <Grid container>
                            <Grid item xs={12}>
                                <Grid container>
                                    <Grid item xs={6}>
                                        <Typography>Order Value:</Typography>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography fontWeight="bold">{selectedOrder.orderValue.toLocaleString()} VND</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid container>
                            <Grid item xs={12}>
                                <Grid container>
                                    <Grid item xs={6}>
                                        <Typography>Order status:</Typography>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography fontWeight="bold">{renderOrderStatus(selectedOrder.orderStatus)}</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                            </Grid>
                            <Grid container>

                            <Grid item xs={12}>
                                <Grid container>
                                    <Grid item xs={6}>
                                        <Typography>Delivery date:</Typography>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <Typography fontWeight="bold">{selectedOrder.orderStatus === 0 ? "Not yet delivered" : selectedOrder.shippedDate}</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                        {orderDetail.map((element, index) => {
                            let orderProduct = getOrderProduct(element)
                            console.log(orderProduct)
                            return <Grid container key={index} style={{paddingTop:"20px", marginRight:"80px"}}>
                                <Grid item xs={4}>
                                    <img style={{ width: "100px", height: "100px" }} src={orderProduct.imageUrl}></img>
                                </Grid>
                                <Grid item xs={8} paddingTop="14px">
                                    <Stack>
                                        <Typography>{orderProduct.brand} {orderProduct.name} {orderProduct.capacity}ML</Typography>
                                        <Typography> Quantity: {element.quantity}</Typography>
                                        <Typography> Product price: {element.priceEach.toLocaleString()}</Typography>
                                    </Stack>
                                </Grid>
                            </Grid>
                        })}
                    </Stack>
                </List>
            </Drawer>
        </div>
    )
}

export default OrderDetail