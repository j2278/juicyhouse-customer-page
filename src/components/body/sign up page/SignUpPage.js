import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux"
import axios from "axios";
import { Button, Grid, Stack, TextField, Typography } from "@mui/material"

const signUpForm = {
    width: "500px",
    margin: "120px auto",
    padding: "0"
}

function SignUpPage() {
    const dispatch = useDispatch();
    const navigation = useNavigate()
    const cart = useSelector(state => state.cart.cart);
    const allCustomer = useSelector(state => state.customer.allCustomer);
    const [errorDisplay, setErrorDisplay] = useState("none")
    const [userInput, setUserInput] = useState({
        fullname: "",
        email: "",
        phone: "",
        address: "",
        city: "",
        country: ""
    })

    const [error, setError] = useState({
        fullname: "Full name is empty!",
        email: "Email is empty!",
        phone: "Phone number is empty!",
        address: "Address is empty!",
        city: "City is empty!",
        country: "Country is empty!"
    })

    const createCustomer = async () => {
        let body = {
            fullname: userInput.fullname,
            email: userInput.email,
            phone: userInput.phone,
            address: userInput.address,
            city: userInput.city,
            country: userInput.country,
            cartList: cart
        }

        const response = await axios(`http://localhost:8000/customers`, {
            method: `POST`,
            data: body,
            header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data;
    }

    const getAllCustomer = async () => {
        const response = await axios(`http://localhost:8000/customers`)
        const data = response.data;
        return data;
    }


    const onFullNameChange = (event) => {
        setUserInput({ ...userInput, fullname: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, fullname: "Full name is empty!" })
        } else {
            setError({ ...error, fullname: "none" })
        }
    }

    const onEmailChange = (event) => {
        setUserInput({ ...userInput, email: event.target.value })
        let emailListExist = allCustomer.filter(element => element.email === event.target.value)

        if (event.target.value === "") {
            setError({ ...error, email: "Email is empty!" })
        } else if (/\S+@\S+\.\S+/.test(event.target.value) === false) {
            setError({ ...error, email: "Email syntax is not valid!" })
        } else if (emailListExist.length > 0) {
            setError({ ...error, email: "This email is already exist!" })
        } else {
            setError({ ...error, email: "none" })
        }
    }

    const onPhoneChange = (event) => {
        setUserInput({ ...userInput, phone: event.target.value })
        let phonelListExist = allCustomer.filter(element => element.phone == event.target.value)

        if (event.target.value === "") {
            setError({ ...error, phone: "Phone number is empty!" })
        } else if (isNaN(event.target.value) || event.target.value.length !== 10) {
            setError({ ...error, phone: "Phone number must be a 10 digit number!" })
        } else if (phonelListExist.length > 0) {
            setError({ ...error, phone: "This phone number is already exist!" })
        } else {
            setError({ ...error, phone: "none" })
        }
    }

    const onAddressChange = (event) => {
        setUserInput({ ...userInput, address: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, address: "Address is empty!" })
        } else {
            setError({ ...error, address: "none" })
        }
    }

    const onCityChange = (event) => {
        setUserInput({ ...userInput, city: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, city: "City is empty!" })
        } else {
            setError({ ...error, city: "none" })
        }
    }

    const onCountryChange = (event) => {
        setUserInput({ ...userInput, country: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, country: "Country is empty!" })
        } else {
            setError({ ...error, country: "none" })
        }
    }

    const createNewAccount = () => {
        setErrorDisplay("block");
        if (error.fullname === "none" && error.email === "none" && error.phone === "none" &&
            error.address === "none" && error.city === "none" && error.country === "none") {
            createCustomer()
                .then((data) => {
                    dispatch({ type: "SET_CUSTOMER", payload: data.newCustomer })
                    dispatch({ type: "SET_CART", payload: data.newCustomer.cartList })
                    navigation("/")
                    getAllCustomer()
                        .then((data) => {
                            dispatch({ type: "SET_ALL_ACCOUNT", payload: data.customerList });
                        })
                })
                .catch((error) => {
                    console.log(error.message)
                })
        }
    }

    const logInClick = () => {
        navigation("/login")
    }

    return (
        <div style={signUpForm}>
            <Typography variant="h6" fontWeight="bolder" className="text-center">Sign Up Form</Typography>
            <Stack spacing={2} style={{ paddingRight: "30px", marginTop: "20px" }}>
                <Grid container>
                    <Grid item xs={12}>
                        <TextField onChange={onFullNameChange} fullWidth size="small" variant="outlined" label="Full name" value={userInput.fullname}></TextField>
                        {error.fullname === "none" ? null : <Typography style={{ color: "red", display: errorDisplay, marginTop: 5 }} variant="caption">{error.fullname}</Typography>}
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={12} lg={8}>
                        <TextField onChange={onEmailChange} style={{ paddingRight: "10px" }} fullWidth size="small" variant="outlined" label="Email" value={userInput.email}></TextField>
                        {error.email === "none" ? null : <Typography style={{ color: "red", display: errorDisplay, marginTop: 5 }} variant="caption">{error.email}</Typography>}
                    </Grid>
                    <Grid item lg={4} xs={12}>
                        <TextField onChange={onPhoneChange} fullWidth size="small" variant="outlined" label="Phone number" value={userInput.phone}></TextField>
                        {error.phone === "none" ? null : <Typography style={{ color: "red", display: errorDisplay, marginTop: 5 }} variant="caption">{error.phone}</Typography>}
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={12}>
                        <TextField onChange={onAddressChange} fullWidth size="small" variant="outlined" label="Address" value={userInput.address}></TextField>
                        {error.address === "none" ? null : <Typography style={{ color: "red", display: errorDisplay, marginTop: 5 }} variant="caption">{error.address}</Typography>}
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item lg={6} xs={12}>
                        <TextField onChange={onCityChange} style={{ paddingRight: "10px" }} fullWidth size="small" variant="outlined" label="City" value={userInput.city}></TextField>
                        {error.city === "none" ? null : <Typography style={{ color: "red", display: errorDisplay, marginTop: 5 }} variant="caption">{error.city}</Typography>}
                    </Grid>
                    <Grid item lg={6} xs={12}>
                        <TextField onChange={onCountryChange} fullWidth size="small" variant="outlined" label="Country" value={userInput.country}></TextField>
                        {error.country === "none" ? null : <Typography style={{ color: "red", display: errorDisplay, marginTop: 5 }} variant="caption">{error.country}</Typography>}
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={12}>
                        <Button fullWidth variant="contained" style={{ fontWeight: "bold" }} onClick={createNewAccount}>Create new account</Button>
                    </Grid>
                </Grid>
                <Typography className="text-center">Already have an account? <span type="button" onClick={logInClick} style={{ color: "blue" }}>Log in here</span></Typography>
            </Stack>
        </div>
    )
}

export default SignUpPage