import { Grid, MenuItem, Stack, TextField } from "@mui/material"
import data from "../../../../data.json"

function FilterTool({ filterInput, filterInputHandler, productData, productDataHandler }) {
    const productType = [
        { typeName: "All Type", typeId: "ALL" },
        { typeName: "Salt Nic Juice", typeId: "SALT-NIC" },
        { typeName: "Free Base Juice", typeId: "FREE-BASE" },
    ]

    const productFlavor = [
        { flavorName: "All Flavor", flavorId: "ALL" },
        { flavorName: "Fruit", flavorId: "Fruit" },
        { flavorName: "Creamy", flavorId: "Creamy" },
        { flavorName: "Beverage", flavorId: "Beverage" },
    ]

    const productCapacity = [
        {capacityMl: "All capacity", capacityId: "ALL"},
        {capacityMl: "10 ML", capacityId: 10},
        {capacityMl: "30 ML", capacityId: 30},
        {capacityMl: "60 ML", capacityId: 60},
        {capacityMl: "100 ML", capacityId: 100},
    ]

    const productOrigin = [
        {originName: "All origin", originId: "ALL"},
        {originName: "England", originId: "England"},
        {originName: "America", originId: "America"},
        {originName: "China", originId: "China"},
        {originName: "France", originId: "France"},
        {originName: "Canada", originId: "Canada"},
    ]

    const onChangeMinPrice = (event) => {
        filterInputHandler({ ...filterInput, minPrice: event.target.value })
    }

    const onChangeMaxPrice = (event) => {
        filterInputHandler({ ...filterInput, maxPrice: event.target.value })
    }

    const onChangeType = (event) => {
        filterInputHandler({ ...filterInput, type: event.target.value })
    }

    const onChangeFlavor = (event) => {
        filterInputHandler({ ...filterInput, flavor: event.target.value })
    }

    const onChangeCapacity = (event) => {
        filterInputHandler({ ...filterInput, capacity: event.target.value })
    }

    const onChangeOrigin = (event) => {
        filterInputHandler({ ...filterInput, origin: event.target.value })
    }


    return (
        <Stack spacing={3}>
            <h5>Filter</h5>
            <Stack>
                <h6>Price</h6>
                <Grid container>
                    <Grid item xs={5}>
                        <TextField size="small" label="Min price" onChange={onChangeMinPrice}></TextField>
                    </Grid>
                    <Grid item xs={1} className="text-center"> - </Grid>
                    <Grid item xs={5}>
                        <TextField size="small" label="Max price" onChange={onChangeMaxPrice}></TextField>
                    </Grid>
                </Grid>
            </Stack>
            <Stack>
                <h6>Type</h6>
                <TextField size="small" select value={filterInput.type} variant="filled" onChange={onChangeType}>
                    {productType.map((element, index) => {
                        return <MenuItem key={index} value={element.typeId}>{element.typeName}</MenuItem>
                    })}
                </TextField>
            </Stack>
            <Stack>
                <h6>Flavor</h6>
                <TextField size="small" select value={filterInput.flavor} variant="filled" onChange={onChangeFlavor}>
                    {productFlavor.map((element, index) => {
                        return <MenuItem key={index} value={element.flavorId}>{element.flavorName}</MenuItem>
                    })}
                </TextField>
            </Stack>
            <Stack >
                <h6>Capacity</h6>
                <TextField size="small" value={filterInput.capacity} select variant="filled" onChange={onChangeCapacity}>
                    {productCapacity.map((element, index) => {
                        return <MenuItem key={index} value={element.capacityId}>{element.capacityMl}</MenuItem>
                    })}
                </TextField>
            </Stack>
            <Stack >
                <h6>Origin</h6>
                <TextField size="small" value={filterInput.origin} select variant="filled" onChange={onChangeOrigin}>
                    {productOrigin.map((element, index) => {
                        return <MenuItem key={index} value={element.originId}>{element.originName}</MenuItem>
                    })}
                </TextField>
            </Stack>
        </Stack>
    )
}

export default FilterTool