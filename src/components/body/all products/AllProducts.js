import { Grid, Pagination, Typography } from "@mui/material"
import BreadcrumComponent from "./breadcrum/Breadcrums"
import FilterTool from "./filter tool/FilterTool"
import ProductList from "./product list/ProductList"
import { useEffect, useState } from "react"
import { useSelector } from "react-redux"
import axios from "axios"

function filterProduct(data, filter) {
    let result = data.filter(element =>
        (filter.minPrice === "" || filter.minPrice <= element.promotionPrice) &&
        (filter.maxPrice === "" || filter.maxPrice >= element.promotionPrice) &&
        (filter.type === "ALL" || filter.type === element.type) &&
        (filter.flavor === "ALL" || filter.flavor === element.flavor) &&
        (filter.capacity === "ALL" || filter.capacity === element.capacity) &&
        (filter.origin === "ALL" || filter.origin === element.productOrigin)
    )
    return result;
}

function AllProducts() {
    const [filterInput, setFilterInput] = useState({
        maxPrice: "",
        minPrice: "",
        type: "ALL",
        flavor: "ALL",
        capacity: "ALL",
        origin: "ALL"
    })

    const getAllProduct = async () => {
        const response = await axios(`http://localhost:8000/products`)
        const data = response.data;
        return data
    }

    const [productInPage, setProductInPage] = useState([])

    const limit = 9

    const [page, setPage] = useState(1)

    const [pageNo, setPageNo] = useState(0)

    const onPageChange = (event, value) => {
        setPage(value)
    }

    useEffect(() => {
        document.title = `Shop24H | All Products`;
    }, [])

    useEffect(() => {
        setPage(1)
    }, [filterInput])

    useEffect(() => {
        getAllProduct()
            .then((data) => {
                let filteredData = filterProduct(data.productList, filterInput)
                setProductInPage(filteredData.slice(limit * (page - 1), limit * page))
                setPageNo(Math.ceil(filteredData.length / limit))
                window.scrollTo(0, 10)
            })
    }, [page, filterInput])

    return (
        <div style={{ paddingTop: "50px" }}>
            <BreadcrumComponent></BreadcrumComponent>
            <Grid container marginTop="50px">
                <Grid item xs={12} lg={3}>
                    <FilterTool
                        filterInput={filterInput} filterInputHandler={setFilterInput}></FilterTool>
                </Grid>
                <Grid item xs={12} lg={9}>
                    <ProductList productInPage={productInPage}></ProductList>
                </Grid>
            </Grid>
            <Grid container justifyContent="flex-end" >
                <Pagination page={page} count={pageNo} size="large" onChange={onPageChange}></Pagination>
            </Grid>
        </div>
    )
}

export default AllProducts