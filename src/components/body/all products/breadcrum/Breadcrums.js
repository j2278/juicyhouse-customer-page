import { Breadcrumbs, Grid, Link, Typography } from "@mui/material"

function BreadcrumComponent() {
    return (
        <div>
            <Grid container>
                <Breadcrumbs separator=">>">
                    <Link underline="hover" color="inherit" href="/">
                        <b>Home</b>
                    </Link>
                    <Typography color="text.primary"><b>All products</b></Typography>
                </Breadcrumbs>
            </Grid>
        </div>
    )
}

export default BreadcrumComponent