import { Card, CardContent, CardMedia, Grid, Typography } from "@mui/material"
import { useNavigate } from "react-router-dom"

function ProductList({ productInPage }) {
    const navigation = useNavigate("")

    const onProductClick = (paramId) => {
        navigation(`/detail/${paramId}`)
    }

    return (
        <Grid container style={{paddingLeft:"50px"}} >
            {
                productInPage.map((element, index) => {
                    return <Card onClick={() => onProductClick(element._id)}  key={index} sx={{ maxWidth: 230 }} style={{ marginRight: "40px", marginBottom: "40px" }}>
                        <CardMedia component="img" height="250" image={element.imageUrl} />
                        <CardContent>
                            <Typography variant="caption" style={{color:"grey", marginBottom:"10px"}}>{element.type} - {element.flavor} - {element.productOrigin} </Typography>
                            <Typography gutterBottom variant="h6" component="div">
                                {element.brand} - {element.name} {element.capacity}ML
                            </Typography>
                            <Grid container direction="row">
                            <Typography variant="body2" color="dark" paddingTop="7px" style={{textDecoration:"line-through"}}>
                                {element.buyPrice.toLocaleString()}
                            </Typography>
                            <Typography variant="h6" style={{color:"red", marginLeft:"20px"}}>
                                {element.promotionPrice.toLocaleString()}
                            </Typography>
                            </Grid>
                        </CardContent>
                    </Card>
                })
            }
        </Grid>
    )
}

export default ProductList