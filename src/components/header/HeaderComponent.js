import { AppBar, Button, Grid, Snackbar, TextField, Alert, Container, Drawer } from "@mui/material"
import { Box } from "@mui/system"
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';
import AccountMenu from "./account menu/AccountMenu";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import "./headerStyle.css";
import MobileMenu from "./mobile menu/MobileMenu";
import PersonIcon from '@mui/icons-material/Person';
import SearchIcon from '@mui/icons-material/Search';

const cartNumberStyle = {
    width: "25px",
    height: "25px",
    backgroundColor: "red",
    borderRadius: "50%",
    position: "relative",
    top: "-4px",
    left: "-30px",
    textAlign: "center"
}

function HeaderComponent() {
    const customer = useSelector(state => state.customer.customer);
    const cart = useSelector(state => state.cart.cart);
    const dispatch = useDispatch();
    const navigation = useNavigate("");
    const [keyword, setKeyword] = useState("");
    const [openSearch, setOpenSearch] = useState(false);

    const onSearchChange = (event) => {
        setKeyword(event.target.value)
    }

    const closeSearch = () => {
        setOpenSearch(false)
    }

    const onSearchClick = () => {
        setOpenSearch(true)
    }

    const onSearchEnter = (event) => {
        if (event.key === "Enter") {
            if (event.target.value !== "") {
                navigation(`/search/${keyword}`)
                setOpenSearch(false)
            }
        }
    }

    const onLogoClick = () => {
        navigation("/")
    }

    const onLoginClick = () => {
        navigation("/login")
    }

    const onCartClick = () => {
        if (cart.length === 0) {
            setOpenAlert(true)
        } else {
            dispatch({ type: "SET_OPEN_CART", payload: true })
        }
    }

    const [openAlert, setOpenAlert] = useState(false);

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenAlert(false);
    };

    return (
        <Container maxWidth="sm">
            <Box>
                <AppBar style={{ backgroundColor: "white", padding: "0" }} >
                    <Grid container>
                        <Grid className="nav-left" item xs={9}  paddingLeft="20px">
                            <Grid container>
                                <Button style={{ color: "black", fontSize: "30px", fontWeight: "bold" }} className="logo" onClick={onLogoClick}  >JUICYHOUSE</Button>
                                <TextField style={{ marginTop: "13px", width: "600px", marginLeft:"20px" }} className="search-bar" onChange={onSearchChange} onKeyDown={onSearchEnter} label="Searching..." variant="outlined" size="small" ></TextField>
                                <SearchIcon onClick={onSearchClick} className="search-icon" fontSize="large" sx={{ color: "black", display: "none"}} />
                            </Grid>
                        </Grid>

                        <Grid item className="nav-right" xs={3}  paddingBottom="10px" paddingRight="50px">
                            <Grid container direction="row" justifyContent="flex-end" paddingTop="18px"  >
                                {customer ? 
                                <MobileMenu onCartClick={onCartClick}></MobileMenu>
                                : <PersonIcon onClick={onLoginClick} fontSize="large" sx={{ color: "black", display: "none"}} className="user-icon" />
                                }

                                {customer ?
                                    <>
                                        <ShoppingCartIcon className="cart-icon" fontSize="large" sx={{ color: "black", marginRight: "20px" }} onClick={onCartClick} />
                                        {cart.length > 0 ?
                                            <div className="cart-number" style={cartNumberStyle}>{cart.length}</div>
                                            : null}
                                        <NotificationsActiveIcon className="noti-icon" fontSize="large" sx={{ color: "black", marginRight: "20px" }} />
                                        <AccountMenu ></AccountMenu>
                                    </>
                                    : <Button className="login-icon" onClick={onLoginClick} style={{ backgroundColor: "black" }} variant="contained">Log In </Button>}
                            </Grid>
                        </Grid>
                    </Grid>
                </AppBar>
            </Box>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="warning" sx={{ width: '100%' }}>
                    Your cart is empty
                </Alert>
            </Snackbar>
            <Drawer open={openSearch} anchor="top" onClose={closeSearch}>
                <TextField onChange={onSearchChange} onKeyDown={onSearchEnter} label="Search"></TextField>
            </Drawer>
        </Container>
    )
}

export default HeaderComponent
