import { Drawer, List, Button, CardMedia, Grid, Stack, Typography, Modal, Box } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "axios"
import CloseIcon from '@mui/icons-material/Close';


function OrderDrawer() {
    const openCart = useSelector(state => state.cart.openCart)
    const cart = useSelector(state => state.cart.cart);
    const total = useSelector(state => state.cart.total);
    const dispatch = useDispatch();
    const customer = useSelector(state => state.customer.customer);
    const navigation = useNavigate()


    const handleClose = () => {
        dispatch({ type: "SET_OPEN_CART", payload: false });
    }

    const removeFromCart = (paramId) => {
        dispatch({ type: "REMOVE_FROM_CART", payload: paramId });
    }

    const reduceBtnClick = (paramId) => {
        dispatch({ type: "REDUCE_AMOUNT", payload: paramId })
    }

    const increaseBtnClick = (paramId) => {
        dispatch({ type: "INCREASE_AMOUNT", payload: paramId })
    }

    const getTotal = () => {
        let total = 0;
        for (let bI = 0; bI < cart.length; bI++) {
            total += (cart[bI].quantity * cart[bI].product.promotionPrice)
        }
        return total;
    }

    const onPaymentClick = () => {
            navigation("/payment")
            handleClose()
    }


    const updateCustomerCart = async () => {
        const response = await axios(`http://localhost:8000/customers/${customer._id}`, {
          method: `PUT`,
          data: {
            cartList: cart
          },
          header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data;
      }

    useEffect(() => {
        if(cart.length === 0) {
            handleClose()
        } 

        if (customer) {
            updateCustomerCart()
              .then((data) => {
                console.log(data);
              })
              .catch((error) => {
                console.log(error.message)
              })
          }
        dispatch({ type: "GET_TOTAL", payload: getTotal() })
    }, [cart])

    return (
        <div>
            <Drawer  open={openCart} anchor="right" onClose={handleClose}>
                <Grid container>
                <Grid item xs={12} sx={{textAlign:"right"}}><CloseIcon  onClick={handleClose}/></Grid>
                </Grid>
                <Typography fontWeight="bolder" className="text-center border-bottom" color="black" variant="h6">
                    CART LIST
                </Typography>
                <List >
                    <Stack spacing={4}>
                        {cart.map((element, index) => {
                            return <Grid container key={index}>
                                <Grid item xs={4}>
                                    <CardMedia component="img" height="200" image={element.product.imageUrl}></CardMedia>
                                </Grid>
                                <Grid item xs={6}>
                                    <Stack style={{ paddingTop: "50px" }} spacing={1}>
                                        <Typography fontWeight="bold">{element.product.brand} {element.product.name} {element.product.capacity}</Typography>
                                        <Typography>
                                            <RemoveCircleIcon onClick={() => reduceBtnClick(element.id)} style={{ paddingBottom: "4px" }}></RemoveCircleIcon>
                                            <span className="mx-3" style={{ fontWeight: "bold", fontSize: "22px" }}>{element.quantity}</span>
                                            <AddCircleIcon onClick={() => increaseBtnClick(element.id)} style={{ paddingBottom: "4px" }}></AddCircleIcon>
                                        </Typography>
                                        <Typography>Price: <span style={{ fontWeight: "bold" }}>{element.product.promotionPrice.toLocaleString()}</span>  </Typography>
                                    </Stack>
                                </Grid>
                                <Button onClick={() => removeFromCart(element.id)} style={{ backgroundColor: "grey", width: "70px", height: "30px", marginTop: "50px", fontWeight: "bolder" }} size="small" variant="contained">Remove</Button>
                            </Grid>
                        })}
                    </Stack>
                </List>
                    <Grid container style={{color:"black", padding:"40px"}}>
                        <Grid item xs={6}>
                            <Typography fontWeight="bold"  variant="h6">Order Total:</Typography>
                        </Grid>
                        <Grid item xs={6} style={{textAlign:"right"}}>
                            <Typography fontWeight="bold" variant="h6">{total.toLocaleString()}</Typography>
                        </Grid>
                        <Grid item xs={12} marginTop="10px">
                            <Button fullWidth onClick={onPaymentClick} style={{ backgroundColor: "black" }} variant="contained">Payment</Button>
                        </Grid>
                    </Grid>

            </Drawer>
        </div>
    )
}

export default OrderDrawer;