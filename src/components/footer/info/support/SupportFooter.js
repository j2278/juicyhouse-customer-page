import { Stack, Typography } from "@mui/material"

function SupportFooter () {
    return (
        <Stack spacing={1} className="text-center">
            <Typography fontWeight="bold" variant="h6">SUPPORT</Typography>
            <Typography variant="body1">Help Center</Typography>
            <Typography variant="body1">Contact Us</Typography>
            <Typography variant="body1">Product Help</Typography>
            <Typography variant="body1">Warrenty</Typography>
            <Typography variant="body1">Order Status</Typography>
        </Stack>
    )
}

export default SupportFooter