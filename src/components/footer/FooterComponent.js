import ProductFooter from "./info/product/ProductFooter"
import ServiceFooter from "./info/service/ServiceFooter"
import SupportFooter from "./info/support/SupportFooter"
import SocialFooter from "./social/SocialFooter"
import { Container, Grid } from "@mui/material"

function FooterComponent() {

    return (
        <div style={{width:"100%", padding:"20px"}}>
            <Grid container>
                <Grid item xs={12}  lg={3}  className="my-2">
                    <ProductFooter></ProductFooter>
                </Grid>
                <Grid item xs={12}  lg={3} className="my-2">
                    <ServiceFooter></ServiceFooter>
                </Grid>
                <Grid item xs={12}  lg={3} className="my-2">
                    <SupportFooter></SupportFooter>
                </Grid>
                <Grid item xs={12}  lg={3} className="my-2">
                    <SocialFooter></SocialFooter>
                </Grid>
            </Grid>
        </div>
    )
}

export default FooterComponent