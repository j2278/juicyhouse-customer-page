import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import YouTubeIcon from '@mui/icons-material/YouTube';
import { Typography, Grid } from "@mui/material"

function SocialFooter() {
    return (
        <div >
            <Typography fontWeight="bolder" variant="h3" className="text-center">JuicyHouse</Typography>
            <Grid container sx={{ paddingTop:"10px"}} >
                <Grid item xs={12} style={{textAlign:"center"}}>
                <FacebookIcon className="mx-2"/>
                <InstagramIcon className="mx-2"/>
                <TwitterIcon className="mx-2"/>
                <YouTubeIcon className="mx-2"/>

                </Grid>
            </Grid>

        </div>
    )
}

export default SocialFooter