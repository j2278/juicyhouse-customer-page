import firebase from "firebase/app"

import "firebase/auth"

const firebaseConfig = {
    apiKey: "AIzaSyB_D0a6319GVGIwjSWsXZ4xooWq0WqdN7g",
    authDomain: "devcamp-r10-2df2b.firebaseapp.com",
    projectId: "devcamp-r10-2df2b",
    storageBucket: "devcamp-r10-2df2b.appspot.com",
    messagingSenderId: "501377149943",
    appId: "1:501377149943:web:190100db7156953c121cf2"
  };

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();

export const googleProvider = new firebase.auth.GoogleAuthProvider();