import { Container } from "@mui/material";
import "bootstrap/dist/css/bootstrap.min.css"
import { Route, Routes } from "react-router-dom";
import AllProducts from "./components/body/all products/AllProducts";
import HomePageComponent from "./components/body/home page/HomePage";
import FooterComponent from "./components/footer/FooterComponent";
import HeaderComponent from "./components/header/HeaderComponent";
import { useEffect } from "react"
import LoginPage from "./components/body/login page/LoginPage";
import { auth } from "./firebase"
import ProductDetail from "./components/body/product detail/ProductDetail";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import SearchResult from "./components/body/searching result/SearchResult";
import { useDispatch } from "react-redux";
import OrderDrawer from "./components/order drawer/OrderDrawer";
import axios from "axios"
import SignUpPage from "./components/body/sign up page/SignUpPage";
import OrderPage from "./components/body/order page/OrderPage";
import PaymentPage from "./components/body/payment/PaymentPage";

function checkCustomer(customerList, userEmail) {
  let result = null;
  let bI = 0;
  let isFound = false;
  while (!isFound && bI < customerList.length) {
    if (userEmail === customerList[bI].email) {
      result = customerList[bI];
      isFound = true;
    } else {
      bI++
    }
  }
  return result
}

function App() {
  const dispatch = useDispatch();

  const getAllCustomer = async () => {
    const response = await axios(`http://localhost:8000/customers`)
    const data = response.data;
    return data;
  }


  const getAllCustomerOrders = async (paramCustomer) => {
    const response = await axios(`http://localhost:8000/customers/${paramCustomer._id}/orders`)
    const data = response.data;
    return data;
  }

  const getAllProduct = async () => {
    const response = await axios(`http://localhost:8000/products`);
    const data = response.data;
    return data;
  }

  useEffect(() => {
    //Gọi all customer để validate khi sign up
    getAllCustomer()
      .then((data) => {
        dispatch({ type: "SET_ALL_ACCOUNT", payload: data.customerList });
        auth.onAuthStateChanged((result) => {
          console.log(result)
          let customerAccount = checkCustomer(data.customerList, result.email);
          if (customerAccount) {
            dispatch({ type: "SET_CUSTOMER", payload: customerAccount });
            dispatch({ type: "SET_CART", payload: customerAccount.cartList });
            getAllCustomerOrders(customerAccount)
              .then((data) => {
                dispatch({ type: "SET_ALL_ORDER", payload: data.customerOrders })
              })
          }
        })
      })

    getAllProduct()
      .then((data) => {
        dispatch({ type: "GET_ALL_PRODUCT", payload: data.productList })
      })
  }, []);

  return (
    <div>
      <HeaderComponent></HeaderComponent>
      <OrderDrawer></OrderDrawer>
      <Container fixed style={{ paddingTop: "65px" }}>
        <Routes>
          <Route exact path="/" element={<HomePageComponent />}></Route>
          <Route exact path="/products" element={<AllProducts />}></Route>
          <Route exact path="/login" element={<LoginPage />}></Route>
          <Route exact path="/signup" element={<SignUpPage />}></Route>
          <Route exact path="/detail/:id" element={<ProductDetail />}></Route>
          <Route exact path="/search/:keyword" element={<SearchResult />}></Route>
          <Route exact path="/payment" element={<PaymentPage />}></Route>
          <Route exact path="/order" element={<OrderPage />}></Route>
        </Routes>
      </Container>
      <Container maxWidth="xl" style={{ backgroundColor: "#CFCFCF", marginTop: "200px" }}>
        <FooterComponent></FooterComponent>
      </Container>
    </div>
  );
}

export default App;
